# Introduction to Python Unit Testing

Hi All!
This is the material for the series of articles at [thisislooca.com](http://thisislooca.com/2018/09/23/introduction-to-python-unit-testing-preface/).

To run the tests, follow the instructions in my website or alternatively install `pytest` using `pip` and run:

```bash
pytest -v
```