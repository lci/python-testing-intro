# test_infra.py
#    Contains tests that make sure that the infrastructure works

def test___infra___alive():
    '''
        A trivial function that always returns true.
        Can be used to check that the test infrastructure works.
    '''
    pass # This test will always pass.
