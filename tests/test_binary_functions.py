# test_binary_functinons.py
#   Contains unit tests for the binary_functions module

import pytest
import random
import sys
from src.binary_functions import to_binary

def test___unit___binary_functions___to_binary___can_be_imported():
    pass # Should always pass if the method can be imported

def test___unit___binary_functions___to_binary___input_0():
    assert to_binary(0) == '0'

def test___unit___binary_functions___to_binary___input_1():
    assert to_binary(1) == '1'

def test___unit___binary_functions___to_binary___input_4():
    assert to_binary(4) == '100'

def test___unit___binary_functions___to_binary___negative_integer():
    seed = random.randint(0, sys.maxsize)
    random.seed(seed)
    try:
        NUM_TESTS = 1000
        for _ in range(NUM_TESTS):
            with pytest.raises(TypeError):
                number = random.randrange(-sys.maxsize, -1)
                to_binary(number)
    except:
        sys.stderr.write(f"[test:unit:binary_functions:to_binary:negative_integer] the seed was {seed}")
        assert False # fail the test
