# binary_functions.py
#    This module contains functions to convert to and from binary.

def to_binary(n):
    '''
        Returns a string containing the binary representation of the passed integer.
    '''

    # Sanitise the input
    if n < 0:
        raise TypeError

    # This will store the individual bits of the output
    bit_list = list()

    # Convert to binary using https://en.wikipedia.org/wiki/Binary_number#Decimal
    while n > 1:
        bit_list.insert(0, n % 2)
        n = int(n / 2)
    bit_list.insert(0, n)

    # Concatenates all elements in the list and returns them
    return ''.join(str(x) for x in bit_list)